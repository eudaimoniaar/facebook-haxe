package flashex.events;

import flash.events.Event;

class LocationChangeEvent extends Event {
	public var location : String;
	public function new(type : String, bubbles : Bool=false, cancelable : Bool=false, ?location : String) : Void {
		super(type, bubbles, cancelable);
		this.location = location;
	}
	public static var LOCATION_CHANGE : String;
	public static var LOCATION_CHANGING : String;
}
