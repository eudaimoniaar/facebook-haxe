/*
Copyright (c) 2010, Adobe Systems Incorporated
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

* Neither the name of Adobe Systems Incorporated nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.facebook.graph.net;


import haxe.Json;
import com.facebook.graph.utils.PostRequest;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
#if FB_ATTACHMENTS
import flash.net.FileReference;
#end
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLVariables;
import flash.utils.ByteArray;

/**
 * Base class used when making requests to the graph API.
 * 
 * 
 */
class AbstractFacebookRequest {

	/**
	 * @private
	 *
	 */
	private var urlLoader:URLLoader;

	/**
	 * @private
	 *
	 */
	private var urlRequest:URLRequest;

	/**
	 * @private
	 *
	 */
	private var _rawResult:String;

	/**
	 * @private
	 *
	 */
	private var _data:Dynamic;

	/**
	 * @private
	 *
	 */
	private var _success:Bool;

	/**
	 * @private
	 *
	 */
	private var _url:String;

	/**
	 * @private
	 *
	 */
	private var _requestMethod:String;

	/**
	 * @private
	 *
	 */
	private var _callback:Dynamic;

	/**
	 * Instantiates a new FacebookRequest.
	 *
	 * @param url The URL to request data from.
	 * Usually will be https://graph.facebook.com.
	 * @param requestMethod The URLRequestMethod
	 * to be used for this request.
	 * <ul>
	 *	<li>GET for retrieving data (Default)</li>
	 * 	<li>POST for publishing data</li>
	 * 	<li>DELETE for deleting objects (AIR only)</li>
	 * </ul>
	 * @param callback Method to call when this request is complete.
	 * The signaure of the handler must be callback(request:FacebookRequest);
	 * Where request will be a reference to this request.
	 */
	public function new():Void {
		
	}

	/**
	 * Returns the un-parsed result from Facebook.
	 * Usually this will be a JSON formatted string.
	 *
	 */
	public var rawResult(get, set):String;
	private function get_rawResult():String {
		return _rawResult;
	}

	private function set_rawResult(rawResult:String):String {
		return _rawResult = rawResult;
	}

	/**
	 * Returns true if this request was successful,
	 * or false if an error occurred.
	 * If success == true, the data property will be the corresponding
	 * decoded JSON data returned from facebook.
	 *
	 * If success == false, the data property will either be the error
	 * from Facebook, or the related ErrorEvent.
	 *
	 */
	public var success(get, set):Bool;
	private function get_success():Bool {
		return _success;
	}

	private function set_success(success:Bool):Bool {
		return _success = success;
	}

	/**
	 * Any resulting data returned from Facebook.
	 * @see #success
	 *
	 */
	public var data(get, null):Dynamic;
	private function get_data():Dynamic {
		return _data;
	}

	public function callURL(callback:Dynamic, url:String = "", locale:String = null):Void {		
		_callback = callback;
		urlRequest = new URLRequest((url.length>0) ? url : _url);
		
		if (locale!=null) {
			var data:URLVariables = new URLVariables();
			data.locale = locale;
			urlRequest.data = data;
		}

		loadURLLoader();
	}

	private function setSuccessCallback(value:Dynamic):Void {
		_callback = value;
	}

	private function isValueFile(value:Dynamic):Bool {
		#if FB_ATTACHMENTS
		return (Std.is(value, FileReference) || Std.is(value, Bitmap) || Std.is(value, BitmapData) || Std.is(value, ByteArray));
		#else
		return (Std.is(value, Bitmap) || Std.is(value, BitmapData) || Std.is(value, ByteArray));
		#end
	}

	private function objectToURLVariables(values:Dynamic):String {
		if (values == null) {
			return "";
			//return new URLVariables();
		}

		var v:Array<String> = Reflect.fields(values);
		var vars:String = "";		
		for (n in v) {
			var val = Reflect.getProperty(values,n);
			vars += n + "=" + val + "&";
		}
		trace("objectToURLVariables: "+vars);

		return vars;
	}

	/**
	 * Cancels the current request.
	 *
	 */
	public function close():Void {
		trace("AbstractFacebookRequest close");
		if (urlLoader != null) {
			urlLoader.removeEventListener(
				Event.COMPLETE,
				handleURLLoaderComplete
			);
			
			urlLoader.removeEventListener(
				IOErrorEvent.IO_ERROR,
				handleURLLoaderIOError
			);
			
			urlLoader.removeEventListener(
				SecurityErrorEvent.SECURITY_ERROR,
				handleURLLoaderSecurityError
			);
			
			try {
				urlLoader.close();
			} catch (e:Dynamic) { }
			
			urlLoader = null;
		}
	}

	/**
	 * @private
	 *
	 */
	private function loadURLLoader():Void {
		urlLoader = new URLLoader();

		urlLoader.addEventListener(Event.COMPLETE, handleURLLoaderComplete,	false, 0, false);
		
		urlLoader.addEventListener(IOErrorEvent.IO_ERROR, handleURLLoaderIOError, false, 0, true);
		
		urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleURLLoaderSecurityError,	false, 0, true);

		//urlRequest.verbose = true;
		
		trace("AbstractFacebookRequest loadURLLoad: "+urlRequest.url);

		urlLoader.load(urlRequest);
	}

	/**
	 * @private
	 *
	 */
	private function handleURLLoaderComplete(event:Event):Void {
		trace("AbstractFacebookRequest handleURLLoaderComplete: "+urlLoader.data + " bytesTotal:"+urlLoader.bytesTotal+" bytesLoaded: "+ urlLoader.bytesLoaded);
		handleDataLoad(urlLoader.data);
	}

	/**
	 * @private
	 *
	 */
	private function handleDataLoad(result:Dynamic,
					  dispatchCompleteEvent:Bool = true
	):Void {
		trace("handleDataLoad: "+result);

		_rawResult = cast(result, String);
		_success = true;
		
		try {
			_data = Json.parse(_rawResult);
		} catch (e:Dynamic) {
			trace("error parsing json result: "+e);
			_data = _rawResult;
			_success = false;
		}
		
		handleDataReady();
		
		trace("AbstractFacebookRequest dispatchComplete: "+_callback);
		if (dispatchCompleteEvent) {
			dispatchComplete();
		}
	}

	/**
	 * @private
	 * 
	 * Called after the loaded data is parsed but before complete is dispatched
	 */
	private function handleDataReady():Void {
		
	}

	/**
	 * @private
	 *
	 */
	private function dispatchComplete():Void {
		if (_callback != null) { _callback(this); }
		close();
	}

	/**
	 * @private
	 *
	 * Facebook will return a 500 Internal ServerError
	 * when a Graph request fails,
	 * with JSON data attached explaining the error.
	 *
	 */
	private function handleURLLoaderIOError(event:IOErrorEvent):Void {
		trace("AbstractFacebookRequest handleURLLoaderIOError");
		_success = false;
		trace("AbstractFacebookRequest: "+event);
		#if flash
		flash.external.ExternalInterface.call("console.log","AbstractFacebookRequest: "+event);
		#end
		_rawResult = (cast(event.target, URLLoader)).data;
		
		if (_rawResult != '') {
			try {
				_data = Json.parse(_rawResult);
			} catch (e:Dynamic) {
				_data = {type:'Exception', message:_rawResult};
				trace("AbstractFacebookRequest handleURLLoaderSecurityError: "+e);


				trace("_rawResult: "+_rawResult);
			}
		} else {
			trace("AbstractFacebookRequest handleURLLoaderSecurityError event: "+event);
			_data = event;
		}
		
		dispatchComplete();
	}

	/**
	 * @private
	 *
	 */
	private function handleURLLoaderSecurityError(event:SecurityErrorEvent):Void {
		trace("AbstractFacebookRequest handleURLLoaderSecurityError");

		_success = false;
		_rawResult = (cast(event.target, URLLoader)).data;
		
		try {
			_data = Json.parse((cast(event.target, URLLoader)).data);
		} catch (e:Dynamic) {
			_data = event;
			trace("AbstractFacebookRequest handleURLLoaderSecurityError: "+e);
		}
		
		dispatchComplete();
	}

	private function extractFileData(values:Dynamic):Dynamic {
		if (values == null) { return null; }

		trace("extractFileData values: "+values);

		//Check to see if there is a file we can upload.
		var fileData:Dynamic = null;
		if (isValueFile(values)) {
			trace("extractFileData isValueFile");
			fileData = values;
		} else if (Reflect.isObject(values)) {
			trace("extractFileData values is an array");
			var v:Array<String> = Reflect.fields(values);
			for (n in v) {
				if (isValueFile(Reflect.field(values, n))) {
					fileData = Reflect.field(values, n);
					Reflect.setField(values, n, null);
					break;
				}
			}
		}
		
		return fileData;
	}

	private function createUploadFileRequest(fileData:Dynamic, values:Dynamic = null):PostRequest {
		trace("createUploadFileRequest");
		var post:PostRequest = new PostRequest();
		
		//Write the primitive values first, if they exist
		if (values!=null) {
			var v:Array<String> = Reflect.fields(values);
			for (n in v) {
				var value = Reflect.getProperty(values,n);
				if(value!=null) {
					trace("value ("+n+", "+value+")");
					post.writePostData(n, value);
				}
			}
		}
		

		//If we have a Bitmap, extract its BitmapData for upload.
		if (Std.is(fileData, Bitmap)) {
			fileData = (cast(fileData, Bitmap)).bitmapData;
		}
		
		if (Std.is(fileData, ByteArray)) {
			//If we have a ByteArray, upload as is.
			post.writeFileData(values.fileName,
				cast(fileData, ByteArray),
				values.contentType
			);
		} else if (Std.is(fileData, BitmapData)) {
			trace("createUploadFileRequest is BitmapData");
			//If we have a BitmapData, create a ByteArray, then upload.
			//var ba:ByteArray = (cast(fileData, BitmapData)).encode("png",1);
			//post.writeFileData( /*values.fileName*/ "source", ba, 'image/png');
		}
		
		post.close();
		urlRequest.contentType = 'multipart/form-data; boundary=' + post.boundary;
		
		return post;
	}

	/**
	 * @return Returns the current request URL
	 * and any parameters being used.
	 *
	 */
	public function toString():String {
		return urlRequest.url +
		((urlRequest.data == null)? '' : '?' + StringTools.htmlUnescape(urlRequest.data.toString()));
	}
}