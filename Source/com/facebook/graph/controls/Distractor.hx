﻿/*
  Copyright (c) 2010, Adobe Systems Incorporated
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:

  * Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

  * Neither the name of Adobe Systems Incorporated nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.facebook.graph.controls;


  import flash.display.Sprite;
  import flash.events.Event;
  import flash.text.TextField;
  import flash.text.TextFormat;

  /**
   * Creates an animated distractor that matches what Facebook uses.
   *
   */
  class Distractor extends Sprite {

/**
 * @private
 *
 */
private var fadeDuration:Float = 800;

/**
 * @private
 *
 */
private var tf:TextField;

/**
 * @private
 *
 */
  private var dots:Sprite;

/**
 * @private
 *
 */
private var dotWidth:Float = 40;

/**
 * @private
 *
 */
  private var dotHeight:Float = 20;

/**
 * @private
 *
 */
  private var totalElapsed:Float = 0;

/**
 * @private
 *
 */
  private var _text:String;

/**
 * Creates a new Distractor instance.
 *
 */
public function new() {
  super();

  configUI();
}

/**
 * Gets or sets the display text.
 *
 */
public var text(getText, setText):String;
 	private function getText():String { return tf.text; }
private function setText(value:String):Void {
  tf.text = value;
  tf.width = tf.textWidth + 10;
  dots.x = tf.width;
}


/**
 * @private
 *
 */
private function handleAddedToStage(event:Event):Void {
  addEventListener(Event.ENTER_FRAME, onTick, false, 0, true);
}

/**
 * @private
 *
 */
private function handleRemovedFromStage(event:Event):Void {
  removeEventListener(Event.ENTER_FRAME, onTick);
}

/**
 * @private
 *
 */
private function clamp(n:Float, min:Float=0, max:Float=1):Float {
  if (n < min){ return min; }
  if (n > max){ return max; }
  return n;
}

/**
 * @private
 *
 */
private function invCos(x:Float):Float {
  return 1 - (0.5 * (Math.cos(x*Math.PI) + 1));
  }

/**
 * @private
 *
 */
private function onTick(event:Event):Void {
  totalElapsed += 20;

  var cycle:Float;
  dots.graphics.clear();

  var fullHeight:Float = dotHeight<<0;
  var restHeight:Float = fullHeight * 0.3<<0;

  var shapeWidth:Float = Math.round((dotWidth/3) * 0.6);
  var shapeGap:Float = Math.round((dotWidth/3) * 0.4);

  var dH:Float = fullHeight - restHeight;

  for (var i:Int=0; i<3; i++){
cycle = (totalElapsed + (3-i)
	* fadeDuration*0.13)
	% fadeDuration;

cycle = invCos(1 - clamp(cycle/(0.6*fadeDuration)));

dots.graphics.lineStyle(1, 0x576EA4, cycle*0.7+0.3, true);
dots.graphics.beginFill(0xB1BAD3, cycle*0.7+0.3);
dots.graphics.drawRoundRect(0.5 + i * (shapeGap + shapeWidth),
		  0.5 + 0.5 * (dH - cycle*dH),
		  shapeWidth,
		  restHeight + cycle * dH,
		  2
		  );
dots.graphics.endFill();
  }
}

/**
 * @private
 *
 */
private function configUI():Void {
  var format:TextFormat = new TextFormat('_sans', 11);

  tf = new TextField();
  tf.selectable = false;
  tf.defaultTextFormat = format;
  addChild(tf);

  dots = new Sprite();
  addChild(dots);
  dots.x = tf.width;

  addEventListener(Event.ADDED_TO_STAGE,
	   handleAddedToStage,
	   false, 0, true
	   );

  addEventListener(Event.REMOVED_FROM_STAGE,
	   handleRemovedFromStage,
	   false, 0, true
	   );
}
  }