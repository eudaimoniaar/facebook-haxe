﻿/*
  Copyright (c) 2010, Adobe Systems Incorporated
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:

  * Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

  * Neither the name of Adobe Systems Incorporated nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.facebook.graph;


import com.facebook.graph.core.AbstractFacebook;
#if FB_BATCH
import com.facebook.graph.data.Batch;
import com.facebook.graph.data.FQLMultiQuery;
#end
import com.facebook.graph.data.FacebookSession;
import com.facebook.graph.net.FacebookRequest;
import com.facebook.graph.utils.FacebookDataUtils;
import com.facebook.graph.utils.IResultParser;
import com.facebook.graph.windows.MobileLoginWindow;

import flash.display.Stage;
import flash.geom.Rectangle;
import flash.net.SharedObject;
import flash.net.URLRequestMethod;

/**
 * For use in Mobile, to access the Facebook Graph API from the Mobile phone.
 *
 */
import flash.errors.Error;

class FacebookMobile extends AbstractFacebook {
	
	#if development
	public static var SO_NAME:String = 'com.facebook.graph.FacebookMobileDevelopment';
	#elseif testing 
	public static var SO_NAME:String = 'com.facebook.graph.FacebookMobileTesting';
	#else
	public static var SO_NAME:String = 'com.facebook.graph.FacebookMobile';
	#end
	public static var _instance:FacebookMobile;
	public static var _canInit:Bool = false;
	private var loginWindow:MobileLoginWindow;
	private var applicationId:String;
	private var loginCallback:Dynamic;
	private var logoutCallback:Dynamic;
	private var initCallback:Dynamic;

	private var webView:WebView;
	public var permissions(default,null):Array<String>;


	/**
	 * Creates a new FacebookMobile instance
	 *
	 */
	public function new() {
		super();

		if (_canInit == false) {
		throw new Error(
			'FacebookMobile is an singleton and cannot be instantiated.'
			);
		}
	}

	/**
	 * Initializes this Facebook singleton with your application ID.
	 * You must call this method first.
	 *
	 * @param applicationId The application ID you created at
	 * http://www.facebook.com/developers/apps.php
	 *
	 * @param callback Method to call when initialization is complete.
	 * The handler must have the signature of callback(success:Dynamic, fail:Dynamic);
	 * Success will be a FacebookSession if successful, or null if not.
	 *
	 * @param accessToken If you have a previously saved access_token, you can pass it in here.
	 *
	 */
	public static function init(applicationId:String,
					callback:Dynamic,
					extendedPermissions:Array<String> = null
	):Void {

		getInstance()._init(applicationId, callback, extendedPermissions);
	}

	public function setSession(session:FacebookSession):Void
	{

		var so:SharedObject = SharedObject.getLocal(SO_NAME);

		so.data.accessToken = session.accessToken;
		so.data.expireDate = session.expireDate;
		so.flush();


		if(verifyAccessToken()) {
			
			initCallback(session,null);
		} else {
			initCallback(null, {needLogin:true});
		}
	}

	public static var locale(null, set):String;

	private static function set_locale(value:String):String {
		return getInstance()._locale = value;
	}

	/**
	 * Opens a new login window so the current user can log in to Facebook.
	 *
	 * @param callback The method to call when login is successful.
	 * The handler must have the signature of callback(success:Dynamic, fail:Dynamic);
	 * Success will be a FacebookSession if successful, or null if not.
	 * 
	 * @param stageRef A reference to the stage
	 *
	 * @param extendedPermissions (Optional) Array of extended permissions
	 * to ask the user for once they are logged in.
	 * 
	 * @param webView (Optional) The instance of StageWebView to use for the login window
	 * 
	 * @param display (Optional) The display type for the OAuth dialog. "wap" for older mobile browsers,
	 * "touch" for smartphones. The Default is "touch".
	 *
	 * For the most current list of extended permissions,
	 * visit http://developers.facebook.com/docs/authentication/permissions
	 *
	 * @see http://developers.facebook.com/docs/authentication
	 * @see http://developers.facebook.com/docs/authentication/permissions
	 * @see http://developers.facebook.com/docs/guides/mobile/
	 *
	 */
	public static function login(callback:Dynamic, extendedPermissions:Array<String>, webView:WebView = null, display:String = 'touch'):Void {
		getInstance()._login(callback, extendedPermissions, webView, display);
	}

	/**
	 * Setting to true (default), this class will manage
	 * the session and access token internally.
	 * Setting to false, no session management will occur
	 * and the end developer must save the session manually.
	 *
	 */
	private static var manageSession:Bool = true;

	private var _manageSession:Bool = true;


	/**
	 * Clears a user's local session.
	 * This method is synchronous, since
	 * its method does not log the user out of Facebook,
	 * only the current application.
	 * 
	 * @param callback (Optional) Method to call when logout is done.
	 * 
	 * @param appOrigin (Optional) The site url specified for your app. Required for clearing html window cookie.
	 *
	 */
	public static function logout(callBack:Dynamic=null, appOrigin:String=null):Void {
		getInstance()._logout(callBack, appOrigin);
	}

	/**
	 * Opens a new window that asks the current user for
	 * extended permissions.
	 * 
	 * @param callback The method to call after request for permissions.
	 * 
	 * @param webView The instance of StageWebView to use.
	 * 
	 * @param extendedPermissions Array of extended permissions to ask the user for once they are logged in.
	 * 
	 *
	 * @see com.facebook.graph.net.FacebookMobile#login()
	 * @see http://developers.facebook.com/docs/authentication/permissions
	 *
	 */
	public static function
		requestExtendedPermissions(callback:Dynamic, webView:WebView, extendedPermissions:Array<String>):Void {
		getInstance()._requestExtendedPermissions(callback, webView, extendedPermissions);
	}

	/**
	 * Makes a new request on the Facebook Graph API.
	 *
	 * @param method The method to call on the Graph API.
	 * For example, to load the user's current friends, pass in /me/friends
	 * @param calllback Method that will be called when this request is complete
	 * The handler must have the signature of callback(result:Dynamic, fail:Dynamic);
	 * On success, result will be the Dynamic data returned from Facebook.
	 * On fail, result will be null and fail will contain information about the error.

	 *
	 * @param params Any parameters to pass to Facebook.
	 * For example, you can pass {file:myPhoto, message:'Some message'};
	 * this will upload a photo to Facebook.
	 * @param requestMethod
	 * The URLRequestMethod used to send values to Facebook.
	 * The graph API follows correct Request method conventions.
	 * GET will return data from Facebook.
	 * POST will send data to Facebook.
	 * DELETE will delete an Dynamic from Facebook.
	 *
	 * @see flash.net.URLRequestMethod
	 * @see http://developers.facebook.com/docs/api
	 *
	 */
	public static function api(method:String,
				   callback:Dynamic,
				   params:Dynamic = null,
				   requestMethod:String = 'GET'
	):Void {

		getInstance()._api(method,
		callback,
		params,
		requestMethod
		);
	}

	/**
	 * Returns a reference to the entire raw Dynamic
	 * Facebook returns (including paging, etc.).
	 *
	 * @param data The result Dynamic.
	 *
	 * @see http://developers.facebook.com/docs/api#reading
	 *
	 */
	public static function getRawResult(data:Dynamic):Dynamic {		
		return getInstance()._getRawResult(data);
	}

	/**
	 * Asks if another page exists
	 * after this result Dynamic.
	 *
	 * @param data The result Dynamic.
	 *
	 * @see http://developers.facebook.com/docs/api#reading
	 *
	 */
	public static function hasNext(data:Dynamic):Bool {
		var result:Dynamic = getInstance()._getRawResult(data);
		if(!result.paging){ return false; }
		return (result.paging.next != null);
	}

	/**
	 * Asks if a page exists
	 * before this result Dynamic.
	 *
	 * @param data The result Dynamic.
	 *
	 * @see http://developers.facebook.com/docs/api#reading
	 *
	 */
	public static function hasPrevious(data:Dynamic):Bool {
		var result:Dynamic = getInstance()._getRawResult(data);
		if(!result.paging){ return false; }
		return (result.paging.previous != null);
	}

	/**
	 * Retrieves the next page that is associated with result Dynamic passed in.
	 *
	 * @param data The result Dynamic.
	 * @param callback Method that will be called when this request is complete
	 * The handler must have the signature of callback(result:Dynamic, fail:Dynamic);
	 * On success, result will be the Dynamic data returned from Facebook.
	 * On fail, result will be null and fail will contain information about the error.
	 * 
	 * @see com.facebook.graph.net.FacebookDesktop#request()
	 * @see http://developers.facebook.com/docs/api#reading
	 *
	 */
	public static function nextPage(data:Dynamic, callback:Dynamic):FacebookRequest {
		return getInstance()._nextPage(data, callback);
	}

	/**
	 * Retrieves the previous page that is associated with result Dynamic passed in.
	 *
	 * @param data The result Dynamic.
	 * @param callback Method that will be called when this request is complete
	 * The handler must have the signature of callback(result:Dynamic, fail:Dynamic);
	 * On success, result will be the Dynamic data returned from Facebook.
	 * On fail, result will be null and fail will contain information about the error.
	 *
	 * @see com.facebook.graph.net.FacebookDesktop#request()
	 * @see http://developers.facebook.com/docs/api#reading
	 *
	 */
	public static function previousPage(data:Dynamic, callback:Dynamic):FacebookRequest {
		return getInstance()._previousPage(data, callback);
	}

	/**
	 * Shortcut method to post data to Facebook.
	 * Alternatively, you can call FacebookMobile.request
	 * and use POST for requestMethod.
	 *
	 * @see com.facebook.graph.net.FacebookMobile#request()
	 */
	public static function postData(method:String,
					callback:Dynamic,
					params:Dynamic = null
	):Void {

		api(method, callback, params, URLRequestMethod.POST);
	}

	/**
	 * Shortcut method to upload video to Facebook.
	 * 
	 * @param method The method to call on the Graph API.
	 * For example, to upload a video, pass in /me/videos. To upload to an friend's wall, pass in /USER_ID/videos.
	 * @param callback Method that will be called when this request is complete
	 * The handler must have the signature of callback(result:Dynamic, fail:Dynamic);
	 * On success, result will be the Dynamic data returned from Facebook.
	 * On fail, result will be null and fail will contain information about the error.
	 * @param params An Dynamic containing the title, description, fileName (including extension), and video (FileReference or ByteArray) 
	 * 
	 * @see http://developers.facebook.com/docs/reference/api/video/
	 * 
	 */
	public static function uploadVideo(method:String, callback:Dynamic = null, params:Dynamic = null):Void {
		getInstance()._uploadVideo(method, callback, params);
	}

	/**
	 * Deletes an Object from Facebook.
	 * The current user must have granted extended permission
	 * to delete the corresponding Object,
	 * or an error will be returned.
	 *
	 * @param method The id and connection of the Object to delete.
	 * For example, /POST_ID/like to remove a like from a message.
	 *
	 * @see http://developers.facebook.com/docs/api#deleting
	 * @see com.facebook.graph.net.FacebookMobile#request()
	 *
	 */
	public static function deleteObject(method:String,
						callback:Dynamic
	):Void {

		getInstance()._deleteObject(method, callback);
	}


	/**
	 * Executes an FQL query on api.facebook.com.
	 * 
	 * @param query The FQL query string to execute.
	 * @param values Replaces string values in the in the query. 
	 * ie. Replaces {digit} or {id} with the corresponding key-value in the values Dynamic 
	 * @see http://developers.facebook.com/docs/reference/fql/
	 * @see com.facebook.graph.net.Facebook#callRestAPI()
	 * 
	 */	
	#if FB_BATCH

	public static function fqlQuery(query:String, callback:Dynamic=null, values:Dynamic=null):Void {
		getInstance()._fqlQuery(query, callback, values);
	}

	/**
	 * Executes an FQL multiquery on api.facebook.com.
	 * 
	 * @param queries FQLMultiQuery The FQL queries to execute.
	 * @param parser IResultParser The parser used to parse result into Dynamic of name/value pairs. 
	 * @see http://developers.facebook.com/docs/reference/fql/
	 * @see com.facebook.graph.net.Facebook#callRestAPI()
	 * 
	 */	
	public static function fqlMultiQuery(queries:FQLMultiQuery, callback:Dynamic=null, parser:IResultParser=null):Void {
		getInstance()._fqlMultiQuery(queries, callback, parser);
	}

	/**
	 * Executes a batch api operation on facebook.
	 * 
	 * @param batch The batch to send to facebook.
	 * @see com.facebook.graph.data.Batch
	 * @see callback The callback to execute when this operation is complete.
	 * 
	 */
	public static function batchRequest(batch:Batch, callback:Dynamic=null):Void {
		getInstance()._batchRequest(batch, callback);
	}

	#end

	/**
	 * Used to make old style RESTful API calls on Facebook.
	 * Normally, you would use the Graph API to request data.
	 * This method is here in case you need to use an old method,
	 * such as FQL.
	 *
	 * @param methodName Name of the method to call on
	 * api.facebook.com (ex: fql.query).
	 * @param values Any values to pass to this request.
	 * @param requestMethod URLRequestMethod used to send data to Facebook.
	 *
	 * @see com.facebook.graph.net.FacebookMobile#request()
	 *
	 */
	public static function callRestAPI(methodName:String,
					   callback:Dynamic = null,
					   values:Dynamic = null,
					   requestMethod:String = 'GET'
	):Void {

		getInstance()._callRestAPI(methodName, callback, values, requestMethod);
	}

	/**
	 * Utility method to format a picture URL,
	 * in order to load an image from Facebook.
	 *
	 * @param id The id you wish to load an image from.
	 * @param type The size of image to display from Facebook
	 * (square, small, or large).
	 *
	 * @see http://developers.facebook.com/docs/api#pictures
	 *
	 */
	public static function getImageUrl(id:String,
					   type:String = null
	):String {

		return getInstance()._getImageUrl(id, type);
	}

	/**
	 * Synchronous call to return the current user's session.
	 *
	 */
	public static function getSession():FacebookSession {
		return getInstance().session;
	}

	private function _init(applicationId:String, initCallback:Dynamic, extendedPermissions:Array<String>):Void {
		
		this.initCallback = initCallback;

		this.applicationId = applicationId;
		this.permissions = extendedPermissions;
		
		session = new FacebookSession();
			
		var so:SharedObject = SharedObject.getLocal(SO_NAME);
		
		#if flash
		session.accessToken = openfl.Lib.current.loaderInfo.parameters.token;
		session.expireDate = ""+(Std.parseFloat(openfl.Lib.current.loaderInfo.parameters.expireDate)+Date.now().getTime());
		#else
		session.accessToken = so.data.accessToken;
		session.expireDate = so.data.expireDate;
		
		#end

		
		
		if(verifyAccessToken()) {
			
			initCallback(session,null);
		} else {
			initCallback(null, {needLogin:true});
		}
	}

	/**
	 * @private
	 *
	 */
	private function verifyAccessToken():Bool {
		
		
		if( (session.accessToken==null) || (session.accessToken!=null && session.accessToken=="") ) {
			return false;
		} else {
			if(!checkExpireDate()) {
				//TODO Fede o Lucho del futuro: corregir
				return true;
				
				session = new FacebookSession();
				return false;
			}
		}

		return true;
	}

	public function needLogin():Bool {
		return !verifyAccessToken();
	}

	private function checkExpireDate(): Bool {
		var expire:Float = Std.parseFloat(session.expireDate);
		var now:Float = Date.now().getTime();

		if(expire<now)
			return true;

		return false;
	}



	/**
	 * @private
	 *
	 */
	private function handleUserLoad(result:Dynamic, error:Dynamic):Void {
		
		if (result!=null) {
			session.uid = result.id;
			session.user = result;
			if (loginCallback != null) {
				loginCallback(session, null);
			}
			if (initCallback != null) {
				initCallback(session, null);
				initCallback = null;
			}
		} else {
			if (loginCallback != null) {
			  loginCallback(null, error);
			}
			if (initCallback != null) {
				initCallback(null, error);
				initCallback = null;
			}
			session = null;
		}
	}
	 
	 /**
	  * @private
	  *
	  */
	private function _login(callback:Dynamic, extendedPermissions:Array<String>, webView:WebView = null, display:String = 'touch'):Void {
		this.loginCallback = callback;
		
		if (webView!=null) {
			this.webView = this.createWebView();
		} else {
			this.webView = webView;
		}

		if (applicationId == null) {
			throw new Error(
				'FacebookMobile.init() needs to be called first.'
			);
		}

		loginWindow = new MobileLoginWindow(handleLogin);
		/*
		var a:Array<Dynamic> = FacebookDataUtils.flattenArray(extendedPermissions);
		var arrayExtendedPermision:Array<String> = new Array<String>();
		for(p in a) arrayExtendedPermision.push(cast(p, String));
		*/
		
		loginWindow.open(this.applicationId, this.webView, extendedPermissions, display);
	}

	/**
	 	 * @private
	 *
	 	 */
	private function _requestExtendedPermissions(
		callback:Dynamic,
		webView:WebView,
		extendedPermissions:Array<String>
	):Void {

		if (applicationId == null) {
			throw new Error(
				'User must be logged in before asking for extended permissions.'
			);
		}

		_login(callback, extendedPermissions, webView);
	}

	/**
	 * @private
	 *
	 */
	private function handleLogin(result:Dynamic, fail:Dynamic):Void {
		/*
		if(result!=null)
			trace("handleLogin result: "+ result);
		else
			trace("handleLogin fail:"+ fail);
		*/
		loginWindow.loginCallback = null;

		if (fail) {
			loginCallback(null, fail);
			return;
		}

		session = new FacebookSession();

		#if flash
		session.accessToken = openfl.Lib.current.loaderInfo.parameters.token;
		session.expireDate = ""+(Std.parseFloat(openfl.Lib.current.loaderInfo.parameters.expireDate)+Date.now().getTime());
		#else
		session.accessToken = result.access_token;
		session.expireDate = ""+(Std.parseFloat(result.expires_in)+Date.now().getTime());
		#end

		
		var so:SharedObject = SharedObject.getLocal(SO_NAME);
		so.data.accessToken = session.accessToken;
		so.data.expireDate = session.expireDate;
		so.flush();

		if(verifyAccessToken())
			loginCallback(session, null);
		else
			loginCallback(null, {needPermissions: true});
	}

	/**
	 * @private
	 *
	 */
	private function _logout(callback:Dynamic=null, appOrigin:String=null):Void {
		this.logoutCallback = callback;
		
		//clears cookie for mobile.
		var params:Dynamic = {};
		params.confirm = 1;
		params.next = appOrigin;
		params.access_token = accessToken;
		var req:FacebookRequest = new FacebookRequest();
		
		openRequests.set(req, handleLogout);
		req.call("https://m.facebook.com/logout.php", "GET" , handleRequestLoad, params);
		
		var so:SharedObject = SharedObject.getLocal(SO_NAME);
		so.clear();
		so.flush();

		session = null;
	}

	/**
	 * @private
	 *
	 */
	private function handleLogout(result:Dynamic, fail:Dynamic):Void {
		//This is a specific case. Since we are hitting a different URL to 
		//logout, we do not get a normal result/fail
		if (logoutCallback != null) {
		logoutCallback(true);
		logoutCallback = null;
		}
	}

	 /**
	 * @private
	 *
	 */
	private function createWebView():WebView {
		if (this.webView!=null) {
			try {
				this.webView.destroy();
			} catch (e:Dynamic) { }
		}
		this.webView = WebView.getInstance();

		return webView;
	}

	 	/**
	 * @private
	 *
	 */
	public static function getInstance():FacebookMobile {
		if (_instance == null) {
			_canInit = true;
			_instance = new FacebookMobile();
			_canInit = false;
		}
		return _instance;
	}
}