﻿/*
  Copyright (c) 2010, Adobe Systems Incorporated
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:

  * Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

  * Neither the name of Adobe Systems Incorporated nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.facebook.graph.utils;

import flash.net.URLVariables;

/**
* Utility class used
* to parse Facebook data into corresponding ActionScript types.
*
*/
class FacebookDataUtils {

/**
* Attempts to convert the various Facebook date formats
* to a Date object.
*
* Supported formats are:
* 1270706413 (unix timestamp), in Seconds
* 2010-03-20T22:46:41+0000 (ISO 8601)
* 11/30/1973
*
*/
public static function stringToDate(value:String):Date  {
	if (value == null) { return null; }

	//###########################
	//Check for different date formats
	//###########################

	//If we have unix timestamp convert to date directly
	var r : EReg = ~/[^0-9]/g;
	if (r.match(value) == false) {
		return Date.fromTime(Std.parseFloat(value));
	}

	//Parse dates in this format: 11/30/1973 or 10/30
	//Most likey will be a users birthday.
	var r2 : EReg = ~/(\d\d)\/(\d\d)(\/\d+)?/ig;
	if (r2.match(value)) {
		var datePeices:Array<String> = value.split('/');
		if (datePeices.length == 3) {
			return new Date(Std.parseInt(datePeices[2]), Std.parseInt(datePeices[0])-1, Std.parseInt(datePeices[1]),0,0,0);
		} else {
			return new Date(0, Std.parseInt(datePeices[0])-1, Std.parseInt(datePeices[1]),0,0,0);
		}
	}

	/*
	Parse dates in this format: 2010-03-20T22:46:41+0000 (ISO 8601)
	http://www.w3.org/TR/NOTE-datetime

	Note this does not match the entire date,
	just that significant portion exists.

	Also does not match smaller format dates,
	only full date / times with time zones
	*/
	var r3 : EReg = ~/\d{4}-\d\d-\d\d[\sT]\d\d:\d\d(:\d\d)?[\.\-Z\+]?(\d{0,4})?(:)?(\-\d\d:)?/ig;
	if (r3.match(value)) {
	return iso8601ToDate(value);
	}

	//We don't know the format, let Date try and parse it.
	return Date.fromString(value);
}

/**
* @private
*
*/
private static function iso8601ToDate(value:String):Date {
	var parts:Array<String> = value.toUpperCase().split('T');

	var date:Array<String> = parts[0].split('-');
	var time:Array<String> = (parts.length <= 1) ? [] : parts[1].split(':');

	var year:UInt = date[0]=='' ? 0 : Std.parseInt(date[0]);
	var month:UInt = date[1]=='' ? 0 : Std.parseInt(date[1]) - 1;
	var day:UInt = date[2]=='' ? 1 : Std.parseInt(date[2]);
	var hour:Int = time[0]=='' ? 0 : Std.parseInt(time[0]);
	var minute:UInt = time[1]=='' ? 0 : Std.parseInt(time[1]);

	var second:UInt = 0;
	var millisecond:UInt = 0;

	if (time[2] != null) {
	var index:Int = time[2].length;

	if (time[2].indexOf('+') > -1) {
		index = time[2].indexOf('+');
	} else if (time[2].indexOf('-') > -1) {
		index = time[2].indexOf('-');
	} else if (time[2].indexOf('Z') > -1) {
		index = time[2].indexOf('Z');
	}

	if (!Math.isNaN(index)) {
		var temp:UInt = Std.parseInt(time[2].substring(0, index));
		second = temp<<0;
		millisecond = Std.int(1000 * ((temp % 1) / 1));
	}

	if (index != time[2].length) {
		var offset:String = time[2].substring(index);
		var userOffset:Int = untyped Date.now().getTimezoneOffset() / 60;
			//new Date(year, month, day, 0, 0, 0).getTimezoneOffset() / 60;

		switch (offset.charAt(0)) {
		case '+' :
		case '-' :
			hour -= userOffset + Std.parseInt(offset.substring(0));
		case 'Z' :
			hour -= userOffset;
		}
	}
	}

	return new Date(year, month, day, hour, minute, second);
}

/**
* Utility method to convert a Date object
* to seconds since Jan 1, 1970 for use on Facebook.
*
*/
public static function dateToUnixTimeStamp(date:Date):Float {
	return date.getTime()/1000;
}

/**
* Converts a multidimensional array into a one dimensional array.
* Used to ensure that ExtendedPermissions
* passed to Facebook are correct.
*
* @param source Array to flatten.
*
*/
public static function flattenArray(source:Array<Dynamic>):Array<Dynamic> {
	if (source == null) { return []; }
	return FacebookDataUtils.internalFlattenArray(source);
}

/**
 *
 * Obtains the query string from the current HTML location
 * and returns its values in a URLVariables instance.
 *
 */
public static function getURLVariables(url:String):URLVariables {
	var params:String = null;

	if (url.indexOf('#') != -1) {
		params = url.substring(url.indexOf('#')+1);
	} else if (url.indexOf('?') != -1) {
		params = url.substring(url.indexOf('?')+1);
	}

	var vars:URLVariables = new URLVariables();
	vars.decode(params);

	return vars;
}

/**
* @private
*
*/
private static function internalFlattenArray(source:Array<Dynamic>,
						destination:Array<Dynamic> = null
						):Array<Dynamic> {

	if (destination == null) { destination = []; }

	var l:UInt = source.length;

	for (i in 0 ... l) {
		var item:Dynamic = source[i];
		if (Std.is(item, Array)) {
			FacebookDataUtils.internalFlattenArray(cast(item, Array<Dynamic>),
							destination
							);
		} else {
			destination.push(item);
		}
	}
	return destination;
}
}