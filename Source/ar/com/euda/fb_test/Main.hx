package ar.com.euda.fb_test;


import openfl.display.Sprite;

import com.facebook.graph.windows.MobileLoginWindow; 
import com.facebook.graph.FacebookMobile;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLVariables;
import flash.net.URLRequest;
import flash.net.URLLoader;
import flash.net.URLRequestMethod;
import flash.display.BitmapData;
import openfl.Assets;

class Main extends Sprite {
	
	private var app_id:String = "564513793669747";
	//private var accessToken:String = "faaec6611d22cefcedc58bee3d3c1b9c";

	var loader:URLLoader;
	var permissions:Array<String> = ["read_stream","publish_actions"];

	public function new () {
		super ();

		FacebookMobile.init(app_id, initCallback, permissions);

		/*
		var params:URLVariables = new URLVariables("access_token=CAAIBbBwkenMBAASADxDkcjFSleDad2TiQQRwZBwattZBp0UMVOzNxKB3dXhrVeOrZBOSO8p2ZA1XR2cRwL3GmpoQzty7NLRyHz0hrVdLLwmmrk0bKKCfMExPh3twv9MFgDFpSy59RqVvoWvrjOXLC6QnZCFZBuereADz5Gk0GktYZAOYhom5qHh");
		var request:URLRequest = new URLRequest("https://graph.facebook.com/v2.0/me/feed"+"?"+"access_token=CAAIBbBwkenMBAASADxDkcjFSleDad2TiQQRwZBwattZBp0UMVOzNxKB3dXhrVeOrZBOSO8p2ZA1XR2cRwL3GmpoQzty7NLRyHz0hrVdLLwmmrk0bKKCfMExPh3twv9MFgDFpSy59RqVvoWvrjOXLC6QnZCFZBuereADz5Gk0GktYZAOYhom5qHh");
		request.data = params;
		//request.url = ;
		//request.url = "https://192.168.0.117/test.json";
		request.method = URLRequestMethod.GET;
		loader = new URLLoader();

		loader.addEventListener(Event.COMPLETE, handleURLLoaderComplete,	false, 0, false);
		loader.addEventListener(IOErrorEvent.IO_ERROR, handleURLLoaderIOError, false, 0, true);
		loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleURLLoaderSecurityError,	false, 0, true);

		loader.load(request);
		*/
	}

	private function handleURLLoaderComplete(e:Event):Void {
		trace("handleURLLoaderComplete: "+e);
	}

	private function handleURLLoaderIOError(e:IOErrorEvent):Void {
		trace("handleURLLoaderIOError: "+e);
	}

	private function handleURLLoaderSecurityError(e:SecurityErrorEvent):Void {
		trace("handleURLLoaderSecurityError: "+e);
	}


	/*
	private function mobileLoginTest() {
		var webView:WebView = WebView.getInstance();
		webView.init(true);

		var loginWindow:MobileLoginWindow = new MobileLoginWindow(loginCallback);

		loginWindow = new MobileLoginWindow(loginCallback);
		loginWindow.open(app_id, webView, [], "touch");
	}

	private function loginCallback(success:Dynamic, fail:Dynamic):Void {
		trace("loginCallback");
		if(success!=null) {
			//FacebookMobile.api('/me', handleMeLoad);
		} else {
			trace("fail: "+fail);
		}
	}
	*/

	private function initCallback(success:Dynamic, fail:Dynamic):Void {
		if(success!=null) {
			trace("Init success: "+success);
			FacebookMobile.api('/me', handleMeLoad);
		} else {
			//Need to login?
			trace("Error init: "+fail);
			if(fail.needLogin==true) {
				FacebookMobile.login(loginCallback , permissions , WebView.getInstance());
			}
		}
	}

	private function loginCallback(success:Dynamic, fail:Dynamic):Void {
		if(success!=null) {
			trace("Login successfull");
			FacebookMobile.api('/me', handleMeLoad);
		} else {
			trace("Login error: "+fail);
		}
	}

	private function postIcon(): Void {
		var attachment:BitmapData =	Assets.getBitmapData("assets/abuela_goye.png");
		var data = {"message":"Estoy jugando Marca Pais icon quiz, ayudame adivinando de quien se trata este icono. https://www.facebook.com/marcaargentina",
					/* "no_story":"true", */
					"source":attachment};
		trace("data: "+data);
		FacebookMobile.postData( "/me/photos" , postImageCallback , data );
	}

	private function postImageCallback(success:Dynamic, fail:Dynamic):Void {
		var photoId;
		if(success!=null) {
			trace("success post: "+success);
			photoId = success.id;
			postWithReference(photoId);
		} else {
			trace("fail post: "+fail);
		}
	}

	private function postWithReference(objectId:Dynamic): Void {
		var privacy:String = "{\"value\":\"ALL_FRIENDS\"}";
		var data = {"message":"Estoy jugando Marca Pais icon quiz, ayudame adivinando de quien se trata este icono.",
					"object_attachment":objectId,
					"privacy":privacy
				};

		trace("data: "+data);
		FacebookMobile.postData( "/me/feed" , postHelpRequestCallback , data );
	}

	private function postHelpRequestCallback(success:Dynamic, fail:Dynamic):Void {
		if(success!=null) {
			trace("success help post: "+success);
		} else {
			trace("fail post: "+fail);
		}
	}

	private function handleMeLoad(success:Dynamic, fail:Dynamic):Void {
		trace("meLoad");
		if(success!=null) {
			trace("success:"+success);
			postIcon();
		} else {
			trace("fail"+fail);
		}
	}
}